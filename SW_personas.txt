------Personas for Semester Project-------
	 1. Administator: 6'0" white guy with a B.S. in Sociology from Pittsburgh
		-age: 42
		-Administrator_Devices: ["iOS Cell Phone", 
		   "GCC Desktop Running Windows 10"]
		-Administartor_ability: Low, prefers functionality 
		   over ease of use or aesthetic
		-Administrator_Goals: "To assist students in course scheduling"
		Administrator_Motivation: "Cheaper and more convenient to use software
  		   made by students here at GCC"
	 2. Student: 5' 6" Freshman Chinese woman from Beijing
		-age: 19
		-Student_Devices: ["Android Cell Phone", "GCC Laptop"]
		-Student_ability: High, since studying Computer Science
		-Student_Major: CS
		Student_Goals: "To manage course scheduling and 
		-resolve potential scheduling conflicts"	
		
		Goals for Both: "Speed.  Want the software to work quickly"
						"A beautiful and intuitive GUI (Graphical User Interface)"