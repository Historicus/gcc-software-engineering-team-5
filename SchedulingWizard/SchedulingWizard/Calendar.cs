﻿////////////////////////////////////////////////
// FileName:          Calendar.cs
// Namespace:         SchedulingWizard
// Project:           SchedulingWizard
// 
// Created On:        4/24/2017
// Last Modified On:  5/03/2017
////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace SchedulingWizard
{
    class Calendar
    {
        List<string> dayButtons = new List<string>();

        //to decide where the button needs to start-determined by start time
        public int getMarginFromTop(Event item)
        {
            int topMargin = (60 * (item.IntstartTimeHour % 8)) + item.IntstartTimeMin;
            if (item.IntstartTimeHour >= 16)
                topMargin += 480;
            return topMargin;
        }

        //to decide where the button needs to end-determined by end time
        public int getMarginFromBottom(Event item)
        {
            int bottomMargin = 780 - (60 * (item.IntendTimeHour - 8)) - item.IntendTimeMin;
            return bottomMargin;
        }

        //determines what days to display the class on-uses day of meeting string from the DB
        public List<string> getDayButtons(string day_of_meeting)
        {
            dayButtons.Clear();
            //start out the list with nothing in it
            dayButtons.Add("null");
            dayButtons.Add("null");
            dayButtons.Add("null");
            dayButtons.Add("null");
            dayButtons.Add("null");

            if (day_of_meeting == null)
                return dayButtons;

            foreach (char curDay in day_of_meeting)
            {
                if (curDay == 'M')
                    dayButtons[0] = "day";
                if (curDay == 'T')
                    dayButtons[1] = "day";
                if (curDay == 'W')
                    dayButtons[2] = "day";
                if (curDay == 'R')
                    dayButtons[3] = "day";
                if (curDay == 'F')
                    dayButtons[4] = "day";
            }

            return dayButtons;
        }

        //check if there is a course conflict with the course being added
        public bool conflictCheck(Event item, List<Event> Events_On_Calendar)
        {
            //get the item start int
            int itemStart = getMarginFromTop(item);
            //calculate the end the same as in getMarginFromTop, but change start time to end time in the calculation.
            int itemEnd = (60 * (item.IntendTimeHour % 8)) + item.IntendTimeMin;
            if (item.IntendTimeHour >= 16)
                itemEnd += 480; ;
            int calStart = 0;
            int calEnd = 0;
            bool sameDay = false;

            for (int i = 0; i < Events_On_Calendar.Count(); i++)
            {
                //makes sure only classes with valid days of meeting are getting checked
                if (item.day_of_meeting == null)
                {
                    return false;
                }
                //check if any of the meeting days are the same
                foreach (char ch in item.day_of_meeting)
                {
                    if (Events_On_Calendar[i].day_of_meeting.Contains(ch))
                    {
                        sameDay = true;
                    }
                }
                if (sameDay)
                {
                    calStart = getMarginFromTop(Events_On_Calendar[i]);
                    calEnd = (60 * (Events_On_Calendar[i].IntendTimeHour % 8)) + Events_On_Calendar[i].IntendTimeMin;
                    if (Events_On_Calendar[i].IntendTimeHour >= 16)
                        calEnd += 480;

                    if (calEnd > itemStart && calStart < itemEnd)
                    {
                        for (int k = 0; k < Events_On_Calendar.Count; k++)
                        {
                            for (int j = 0; j < Events_On_Calendar[k].buttons.Count; j++)
                            {
                                if (item.course_code != Events_On_Calendar[k].course_code)
                                    Events_On_Calendar[k].buttons[j].IsEnabled = false;
                            }
                        }
                        return true;
                    }
                }
                sameDay = false;
            }
            return false;
        }
    }
}
