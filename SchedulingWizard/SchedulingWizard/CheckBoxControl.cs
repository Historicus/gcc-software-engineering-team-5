﻿////////////////////////////////////////////////
// FileName:          CheckBoxControl.cs
// Namespace:         SchedulingWizard
// Project:           SchedulingWizard
// 
// Created On:        4/24/2017
// Last Modified On:  5/02/2017
////////////////////////////////////////////////


using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace SchedulingWizard
{
    class CheckBoxControl
    {
        MainWindow Form = null;

        public void Setup() {
            Form = Application.Current.Windows[0] as MainWindow;
        }

        public void Reapply_Checkbox_Filters()
        {
            if ((bool)Form.TTh_Checkbox.IsChecked)
            {
                Filter_By_TR();
            }
            if ((bool)Form.MWF_Checkbox.IsChecked)
            {
                Filter_By_MWF();
            }
        }

        public void Filter_By_MWF()
        {
            // List of events that do not occur on either M, W, or F and which will be removed from the course list
            List<ListViewItem> removals = new List<ListViewItem>();
            foreach (ListViewItem evnt in Form.List_Of_Classes.Items)
            {
                // Converts the ListViewItem displayed on the GUI to its corresponding Event object
                Event temp_event = (Event)evnt.Tag;
                if (temp_event.day_of_meeting == null ||
                    !((temp_event.day_of_meeting.Contains("M")) ||
                      (temp_event.day_of_meeting.Contains("W")) ||
                      (temp_event.day_of_meeting.Contains("F")))
                    )
                {
                    // Adds all events that do not meet on M, W, or F to the removal list
                    removals.Add(evnt);
                }

            }
            Update_List_by_Checkbox(removals);
            return;
        }

        public void Unfilter_By_MWF()
        {
            Form.Initiate_Query("keyDown");

            // Simulates the user checking the TR checkbox if the TR checkbox is already checked
            // This way the TR filter is re-applied if necessary
            if ((bool)Form.TTh_Checkbox.IsChecked)
            {
                Filter_By_TR();
            }

            // Resorts the list of classes so they appear in alphabetical order
            Form.Sort_List_Of_Classes();

            return;
        }

        public void Filter_By_TR()
        {
            // List of events that do not occur on either Tuesday or Thursday and which will be removed from the course list
            List<ListViewItem> removals = new List<ListViewItem>();

                foreach (ListViewItem evnt in Form.List_Of_Classes.Items)
            {
                // Converts the ListViewItem displayed on the GUI to its corresponding Event object
                Event temp_event = (Event)evnt.Tag;
                if (temp_event.day_of_meeting == null ||
                    !((temp_event.day_of_meeting.Contains("T")) ||
                      (temp_event.day_of_meeting.Contains("R"))))
                {
                    // Adds all events that do not meet on Tuesday or Thursday to the removal list
                    removals.Add(evnt);
                }
            }
            Update_List_by_Checkbox(removals);
            return;
        }

        public void Unfilter_By_TR()
        {
            Form.Initiate_Query("keyDown");

            // Simulates the user checking the MWF checkbox if the MWF checkbox is already checked
            // This way the MWF filter is re-applied if necessary
            if ((bool)Form.MWF_Checkbox.IsChecked)
            {
                Filter_By_MWF();
            }

            // Resorts the list of classes so they appear in alphabetical order
            Form.Sort_List_Of_Classes();

            return;
        }

        // Given a list of ListViewItems that should be removed from the list, the List_Of_Classes will be updated
        // Recitations will also be added and the list will be sorted
        public void Update_List_by_Checkbox(List<ListViewItem> removals)
        {
            // Loops through the list of events to be removed (i.e. filtered out) and removes them from the list of courses on the GUI
            foreach (ListViewItem item in removals)
            {
                Form.List_Of_Classes.Items.Remove(item);
                Event evnt = (Event)item.Tag;
                Form.inList.Remove(evnt);
            }

            // Creates a list to hold events that need to be added to the list of courses (e.g. recitations)
            List<Event> additions = new List<Event>();
            foreach (Event evnt_in_list in Form.inList)
            {
                // Adds all events in the database which match the course code of an event already in the list of courses
                foreach (Event any_evnt in Form.DB)
                {
                    if ((evnt_in_list.course_code == any_evnt.course_code) &&
                        (evnt_in_list.day_of_meeting != any_evnt.day_of_meeting) &&
                        (evnt_in_list.recitation == true) &&
                        (any_evnt.recitation == true) &&
                        !(Form.inList.Contains(any_evnt)))
                    {
                        additions.Add(any_evnt);
                    }
                }
            }
            foreach (Event evnt in additions)
            {
                Form.Add_Event_to_List_Of_Classes(evnt); // recitations are added to the GUI
                Form.inList.Add(evnt); // recitations are added to internal list of classes displayed on the GUI
            }

            // Resorts the list of classes so they appear in alphabetical order
            Form.Sort_List_Of_Classes();
            return;
        }
    }
}
