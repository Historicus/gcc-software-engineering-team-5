Notes for our Requiements:
1. Item ID 10 says that "As a user, I want to be able to export all my courses into a easily readable document." Instead of exporting to a easily readable
	document we exported to an xml file that could be imported. Importing would allow classes saved in export to be shown on the calendar as they were during the export.
2. Item ID 8 says "A remote Linux Raspbian server is used to store coursedata using MySQL or SQLite." Instead of using a Linux Raspbian server we used a dedicated ubuntu
    server running MySQL to host our Database