﻿////////////////////////////////////////////////
// FileName:          TimeUnitTests.cs
// Namespace:         SchedulingWizard
// Project:           SchedulingWizardUnitTests
// 
// Created On:        5/05/2017
// Last Modified On:  5/06/2017
////////////////////////////////////////////////


using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Runtime.Serialization;
using Microsoft.Win32;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using CourseDBContext;
using System.Linq;
using SchedulingWizard;

namespace SchedulingWizardUnitTests
{
    /// <summary>
    /// Summary description for TimeUnitTests
    /// </summary>
    [TestClass]
    public class TimeUnitTests
    {
        Event testEvent = new Event("crs1", "course1", "CRS1", "100", "BLD", "8:00", "9:00", "MWF", 0, 10);
        string[] civilianTime;
        string testStartTime;
        string testEndTime;

        //tests only times that would be formatting in AM
        [TestMethod]
        public void CivilianTimeMorningTestMethod()
        {
            testStartTime = "9:00:00";
            testEndTime = "9:30:00";
            testEvent.time_to_int(testStartTime, testEndTime);
            civilianTime = testEvent.getCivilianTime(testStartTime, testEndTime);
            Assert.AreEqual("9:00 AM", civilianTime[0]);
            Assert.AreEqual("9:30 AM", civilianTime[1]);

            testStartTime = "8:00:00";
            testEndTime = "9:00:00";
            testEvent.time_to_int(testStartTime, testEndTime);
            civilianTime = testEvent.getCivilianTime(testStartTime, testEndTime);
            Assert.AreEqual("8:00 AM", civilianTime[0]);
            Assert.AreEqual("9:00 AM", civilianTime[1]);

            testStartTime = "10:00:20";
            testEndTime = "11:00:71";
            testEvent.time_to_int(testStartTime, testEndTime);
            civilianTime = testEvent.getCivilianTime(testStartTime, testEndTime);
            Assert.AreEqual("10:00 AM", civilianTime[0]);
            Assert.AreEqual("11:00 AM", civilianTime[1]);

            testStartTime = "3:00:00";
            testEndTime = "11:05:00";
            testEvent.time_to_int(testStartTime, testEndTime);
            civilianTime = testEvent.getCivilianTime(testStartTime, testEndTime);
            Assert.AreEqual("3:00 AM", civilianTime[0]);
            Assert.AreEqual("11:05 AM", civilianTime[1]);
        }

        //tests only times that would be formatting in PM
        [TestMethod]
        public void CivilianTimeAfternoonTestMethod()
        {
            testStartTime = "11:59:00";
            testEndTime = "12:01:00";
            testEvent.time_to_int(testStartTime, testEndTime);
            civilianTime = testEvent.getCivilianTime(testStartTime, testEndTime);
            Assert.AreEqual("11:59 AM", civilianTime[0]);
            Assert.AreEqual("12:01 PM", civilianTime[1]);

            testStartTime = "18:30:10";
            testEndTime = "19:15:00";
            testEvent.time_to_int(testStartTime, testEndTime);
            civilianTime = testEvent.getCivilianTime(testStartTime, testEndTime);
            Assert.AreEqual("6:30 PM", civilianTime[0]);
            Assert.AreEqual("7:15 PM", civilianTime[1]);

            testStartTime = "12:00:00";
            testEndTime = "20:00:00";
            testEvent.time_to_int(testStartTime, testEndTime);
            civilianTime = testEvent.getCivilianTime(testStartTime, testEndTime);
            Assert.AreEqual("12:00 PM", civilianTime[0]);
            Assert.AreEqual("8:00 PM", civilianTime[1]);
        }

        //tests times that would be formatting in both AM and PM, possibly overlapping
        [TestMethod]
        public void CivilianTimeMixedTestMethod()
        {
            testStartTime = "1:00:59";
            testEndTime = "21:05:00";
            testEvent.time_to_int(testStartTime, testEndTime);
            civilianTime = testEvent.getCivilianTime(testStartTime, testEndTime);
            Assert.AreEqual("1:00 AM", civilianTime[0]);
            Assert.AreEqual("9:05 PM", civilianTime[1]);

            testStartTime = "12:00:59";
            testEndTime = "21:05:00";
            testEvent.time_to_int(testStartTime, testEndTime);
            civilianTime = testEvent.getCivilianTime(testStartTime, testEndTime);
            Assert.AreEqual("12:00 PM", civilianTime[0]);
            Assert.AreEqual("9:05 PM", civilianTime[1]);

            testStartTime = "1:03:59";
            testEndTime = "12:00:00";
            testEvent.time_to_int(testStartTime, testEndTime);
            civilianTime = testEvent.getCivilianTime(testStartTime, testEndTime);
            Assert.AreEqual("1:03 AM", civilianTime[0]);
            Assert.AreEqual("12:00 PM", civilianTime[1]);

            testStartTime = "11:03:59";
            testEndTime = "11:03:59";
            testEvent.time_to_int(testStartTime, testEndTime);
            civilianTime = testEvent.getCivilianTime(testStartTime, testEndTime);
            Assert.AreEqual("11:03 AM", civilianTime[0]);
            Assert.AreEqual("11:03 AM", civilianTime[1]);

            testStartTime = "16:53:09";
            testEndTime = "16:53:09";
            testEvent.time_to_int(testStartTime, testEndTime);
            civilianTime = testEvent.getCivilianTime(testStartTime, testEndTime);
            Assert.AreEqual("4:53 PM", civilianTime[0]);
            Assert.AreEqual("4:53 PM", civilianTime[1]);

            testStartTime = "16:53:09";
            testEndTime = "1:53:09";
            testEvent.time_to_int(testStartTime, testEndTime);
            civilianTime = testEvent.getCivilianTime(testStartTime, testEndTime);
            Assert.AreEqual("4:53 PM", civilianTime[0]);
            Assert.AreEqual("1:53 AM", civilianTime[1]);

            testStartTime = "12:03:09";
            testEndTime = "12:01:09";
            testEvent.time_to_int(testStartTime, testEndTime);
            civilianTime = testEvent.getCivilianTime(testStartTime, testEndTime);
            Assert.AreEqual("12:03 PM", civilianTime[0]);
            Assert.AreEqual("12:01 PM", civilianTime[1]);

            testStartTime = "9:53:00";
            testEndTime = "7:30:00";
            testEvent.time_to_int(testStartTime, testEndTime);
            civilianTime = testEvent.getCivilianTime(testStartTime, testEndTime);
            Assert.AreEqual("9:53 AM", civilianTime[0]);
            Assert.AreEqual("7:30 AM", civilianTime[1]);
        }
    }
}
